#!/bin/bash

MASTER_STATUS=$(docker exec db-master sh -c 'export MYSQL_PWD='"$DB_PASSWORD"'; mysql -u '"$DB_USERNAME"' -e "SHOW MASTER STATUS"')
CURRENT_LOG=$(echo $MASTER_STATUS | awk '{print $6}')
CURRENT_POS=$(echo $MASTER_STATUS | awk '{print $7}')

echo $MASTER_STATUS
echo $CURRENT_LOG
echo $CURRENT_POS

result=$(docker exec db-slave sh -c "export MYSQL_PWD='"$DB_PASSWORD"'; mysql -u '"$DB_USERNAME"' -e 'SHOW MASTER STATUS \G'")
if [[ $result ]]; then
    echo "OK"
else
    echo "FAIL"
fi