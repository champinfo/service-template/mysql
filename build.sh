#!/bin/bash

docker-compose down
docker image prune -af

docker-compose up -d --build db-mastter db-slave

until docker exec db-master sh -c 'export MYSQL_PWD='"$DB_PASSWORD"'; mysql -u '"$DB_USERNAME"' -e ";"'
do
    echo "Waiting for db-master database connection...with $DB_USERNAME and $DB_PASSWORD"
    sleep 4
done

until docker exec db-slave sh -c 'export MYSQL_PWD='"$DB_PASSWORD"'; mysql -u '"$DB_USERNAME"' -e ";"'
do
    echo "Waiting for db-slave database connection...with $DB_USERNAME and $DB_PASSWORD"
    sleep 4
done

docker-ip() {
    docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "$@"
}

result=$(docker exec db-slave sh -c "export MYSQL_PWD=$DB_PASSWORD; mysql -u $DB_USERNAME -e 'SHOW SLAVE STATUS \G'")
if [[ $result ]]; then
    echo "Already setup replica, restart slave..."
    docker exec db-slave sh -c 'export MYSQL_PWD='"$DB_PASSWORD"'; mysql -u '"$DB_USERNAME"' -e "STOP SLAVE;RESET SLAVE"'
else
    echo "Setup replica initializing..."
fi

# shellcheck disable=SC2006
MASTER_STATUS=$(docker exec db-master sh -c 'export MYSQL_PWD='"$DB_PASSWORD"'; mysql -u '"$DB_USERNAME"' -e "SHOW MASTER STATUS"')
CURRENT_LOG=$(echo $MASTER_STATUS | awk '{print $6}')
CURRENT_POS=$(echo $MASTER_STATUS | awk '{print $7}')
MASTER_IP=$(docker-ip db-master)

echo $MASTER_IP

start_slave_stmt="CHANGE MASTER TO MASTER_HOST='$MASTER_IP',MASTER_USER='$DB_USERNAME',MASTER_PASSWORD='$DB_PASSWORD',MASTER_LOG_FILE='$CURRENT_LOG',MASTER_LOG_POS=$CURRENT_POS; START SLAVE;"
start_slave_cmd='export MYSQL_PWD='"$DB_PASSWORD"'; mysql -u '"$DB_USERNAME"' -e "'
start_slave_cmd+="$start_slave_stmt"
start_slave_cmd+='"'
docker exec db-slave sh -c "$start_slave_cmd"
docker exec db-slave sh -c "export MYSQL_PWD=$DB_PASSWORD; mysql -u $DB_USERNAME -e 'SHOW SLAVE STATUS \G'"
echo "Setup replica ending..."