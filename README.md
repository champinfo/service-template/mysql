# 說明

目前docker-compose的context路徑設定到此repo的上一層, 作為submodule來看，如果有需要的話記得修改context設定值

在執行`build.sh`之類的建立資料庫之前，需要先執行

1. 設定shell script使用的變數，直接讀取.env檔案

```shell
export $(cat .env | xargs)
```

在.env檔案內，我們設定了database用到的參數，docker network以及env檔案路徑

2. 在此`submodule`目錄上一層
    * 參考`example.init.sql`來建立`init.sql`
    * 參考`.env.example`來建立環境檔`.env`

3. 在`docker-compose.yml'內主要的service, 新增

```shell
depends_on:
      db-slave:
        condition: service_healthy
```
