FROM mysql:5.7

ARG IMAGE_MASTER_TAG

ADD ./mysql/mysql-common.cnf /etc/mysql/conf.d/mysql-server-comm.cnf
ADD ./mysql/master-prod.cnf /etc/mysql/conf.d/master.cnf
ADD ./init.sql /docker-entrypoint-initdb.d/